const dotenv = require('dotenv'),
      http = require('http'),
      express = require('express'),
      oracledb = require('oracledb');

const app = require('./app');
const router = express.Router();

const PORT = process.env.PORT || 7800;

// Load environment variables
dotenv.config({ path: './.env' });

//database connection
// let connection;
//
// (async function() {
// try{
//    connection = await oracledb.getConnection({
//         user : 'priya',
//         password : 'oracle',
//         connectString : 'localhost/XE'
//    });
//    console.log("Successfully connected to Oracle!")
// } catch(err) {
//     console.log("Error: ", err);
//   } finally {
//     if (connection) {
//       try {
//         await connection.close();
//       } catch(err) {
//         console.log("Error when closing the database connection: ", err);
//       }
//     }
//   }
// })()


//server
const server = http.createServer(app).listen(PORT, function() {
    console.log('Server listening on port ' + PORT);
});
