const express = require('express'),
      mysql = require('mysql'),
      oracledb = require('oracledb'),
      bodyParser = require('body-parser');
      Sequelize = require('sequelize-oracle');

const userRouter = require('./routes/userRoute.js');
const authController = ('./controllers/authController');



const NODE_ENV = process.env.NODE_ENV;

const app = express();
const router = express.Router();

var connAttrs = {
  "user" : 'priya',
  "password" : 'oracle',
  "connectString" : 'localhost/XE'
}

let connection;

(async function() {
try{
   connection = await oracledb.getConnection({
        user : 'priya',
        password : 'oracle',
        connectString : 'localhost/XE'
   });
   console.log("Successfully connected to Oracle!")
} catch(err) {
    console.log("Error: ", err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch(err) {
        console.log("Error when closing the database connection: ", err);
      }
    }
  }
})()

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//routes
app.post('/', (req, res) => {
    res
    .status(200)
    .json({
      message:'This is an authentication server'});
});

app.post('/signup',function( req, res ){
        signupHandler( req, res);
    });

function signupHandler(req, res){
"use strict";
if ("application/json" !== req.get('Content-Type')) {
    res.set('Content-Type', 'application/json').status(415).send(JSON.stringify({
        status: 415,
        message: "Wrong content-type. Only application/json is supported",
        detailed_message: null
    }));
    return;
}
oracledb.getConnection(connAttrs, function (err, connection) {
    if (err) {
        // Error connecting to DB
        res
        .set('Content-Type', 'application/json')
        .status(500)
        .send(JSON.stringify({
            status: 500,
            message: "Error connecting to DB",
            detailed_message: err.message
        }));
        return;
    }
    connection.execute("INSERT INTO registered_users VALUES " +
        "(:firstname, :lastname,:username,:email,:password,:phonenumber,:companyname,:companylocation,:pincode,:servertype,:domainurl)" , [req.body.firstname, req.body.lastname,req.body.username,req.body.email,req.body.password,req.body.phonenumber,req.body.companyname,req.body.companylocation,req.body.pincode,req.body.servertype,req.body.domainurl], {
            autoCommit: true,
            outFormat: oracledb.OBJECT // Return the result as Object
        },
        function (err, result) {
            if (err) {
                // Error
                res.set('Content-Type', 'application/json');
                res.status(400).send(JSON.stringify({
                    status: 400,
                    message: err.message.indexOf("ORA-00001") > -1 ? "User already exists" : "Input Error",
                    detailed_message: err.message
                }));
            } else {
                // Successfully created the resource
                res.status(201).set('Location', '/signup' + req.body.name).end();
            }
            // Release the connection
            connection.release(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    } else {
                        console.log("POST /signup : Connection released");
                    }
                });
        });
});

};

module.exports = app;
