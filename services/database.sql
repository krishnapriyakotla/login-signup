CREATE DATABASE IF NOT EXISTS harrinidb;



create table registered_users(
firstname varchar2(50),
lastname varchar2(50),
username varchar2(50),
email varchar2(100),
password varchar2(10),
phonenumber number(15),
companyname varchar2(50),
companylocation varchar2(30),
pincode number(7),
servertype varchar2(30),
domainurl varchar2(100) not null
);
