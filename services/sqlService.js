const       Sequelize = require('sequelize'),
            util      = require('util'),
            path = require('path'),
            sequelize = new Sequelize('database','priya','oracle', {
            host: process.env['MYSQL_HOST'],
            port: process.env['MYSQL_PORT'],
      	    dialect: 'mysql',
                  logging: false
            }),
            moment = require('moment');

let users = sequelize.define('users', {
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    phone: Sequelize.STRING,
    token: Sequelize.STRING,

});
let sendExports = {
    insert: function( table, createObj, callback ) {
        sequelize.sync().then(function() {
            return table.create( createObj );
        }).then(function( data ) {
            callback({
                error: false,
                data: data
            })
        }, function( err ){
            callback({
                error: true,
                data: err
            })
        });
    },
    findOrCreate: function( table, whereObj, defaultsObj, callback ) {
        table
            .findOrCreate({where: whereObj, defaults: defaultsObj })
            .then(function( data ) {
                callback({
                    error: false,
                    data: data
                })
            }, function( err ){
                callback({
                    error: true,
                    data: err
                })
            });
    },
    findAll: function( table, whereObj, callback ) {
        let order;
        if(whereObj.order){
            delete whereObj.order;
            order = [['createdAt', 'DESC']];
        } else {
            delete whereObj.order;
            order = [['createdAt', 'ASC']];
        }
        table.findAll({ where: whereObj, order: order }).then(function(projects) {
            callback({
                error: false,
                data: projects
            })
        }, function( err ){
            callback({
                error: true,
                data: err
            })
        });
    },
    findPagination: function( table, whereObj, limit, offset, callback ) {
        table.findAll({ where: whereObj, order: [['createdAt', 'DESC']], offset: offset, limit: limit }).then(function(projects) {
            callback({
                error: false,
                data: projects
            })
        }, function( err ){
            callback({
                error: true,
                data: err
            })
        });
    },
    findOne: function( table, whereObj, callback ) {
        table.findAll({ limit: 1, where: whereObj }).then(function(projects) {
            let sendProjects;
            if( projects[0] ) {
                sendProjects = projects[0];
            } else {
                sendProjects = {};
            }
            callback({
                error: false,
                data: sendProjects
            })
        }, function( err ){
            callback({
                error: true,
                data: err
            })
        });
    },
    findOneDesc: function( table, whereObj, callback ) {
        if(whereObj.order == false){
            delete whereObj.order;
        } else {
            let order = [['createdAt', 'DESC']];
        }
        table.findAll({ limit: 1, where: whereObj, order: order }).then(function(projects) {
            let sendProjects;
            if( projects[0] ) {
                sendProjects = projects[0];
            } else {
                sendProjects = {};
            }
            callback({
                error: false,
                data: sendProjects
            })
        }, function( err ){
            callback({
                error: true,
                data: err
            })
        });
    },
    update: function( table, newObj, whereObj, callback ) {
        table.update(newObj, { where: whereObj }).then(function() {
            callback({
                error: false
            })
        }, function( err ){
            callback({
                error: true,
                data: err
            })
        });
    },
    delete: function( table, whereObj, callback ) {
        table.destroy({
            where: whereObj
        }).then(function() {
            callback({
                error: false
            });
        }, function( err ) {
            callback({
                error: true,
                data: err
            })
        });
    },

    count: function(table, name, callback) {
        table.findAndCountAll({attributes:[ name ],group: name}).then(function(result) {
            callback({
                error: false,
                data: result
            })
        }, function(err) {
            callback({
                error: true,
                data: err
            })
        });
    },
    _findUsers: function( deliveryId, cb ) {
        sendExports.findOneDesc(Users, {id: deliveryId}, function(response){
            if( response.error ) {
                sendExports._findUsers( deliveryId, cb );
            } else {
                if(response && response.data && response.data.dataValues && response.data.dataValues.id){
                    cb({
                        askDetails: "false"
                    });
                } else {
                    cb({
                        askDetails: "true"
                    });
                }
            }
        });
    },
    _updateRealTime: function( table, findObj, obj, cb ) {
        sendExports.update(table, obj, findObj, function(result){
            if( result.error ) {
                sendExports._updateRealTime( table, findObj, obj, cb );
            } else {
                cb( result );
            }
        });
    },
    _createRealTime: function( table, obj, cb ) {
        sendExports.insert( table, obj, function( result ){
            if( result.error ) {
                sendExports._createRealTime( table, obj, cb );
            } else {
                cb( result );
            }
        });
    },
    _findOrCreateRealTime: function( table, findObj, deliveryId, obj, cb ) {
        sendExports.findOne(table, findObj, function(response){
            if( response.error ) {
                sendExports._findOrCreateRealTime( table, findObj, deliveryId, obj, cb );
            } else {
                if (response && response.data && response.data.dataValues && response.data.dataValues.id) {
                    //Update User
                    sendExports._updateRealTime( table, findObj, obj, cb );
                } else {
                    //Create User
                    sendExports._createRealTime( table, obj, cb );
                }
            }
        });
    },
    _returnRealTime: function( table, findObj, cb ) {
        sendExports.findOne(table, findObj, function(response){
            if( response.error ) {
                cb( false, {
                    data: 'Database error!'
                });
            } else {
                if (response && response.data && response.data.dataValues && response.data.dataValues.id) {
                    cb( true, response );
                } else {
                    cb( false, response );
                }
            }
        });
    },
    _returnAllRealTime: function( table, findObj, cb ) {
        sendExports.findAll(table, findObj, function(response){
            if( response.error ) {
                cb( false, {
                    data: 'Database error!'
                });
            } else {
                if (response && response.data && response.data.length > 0 ) {
                    cb( true, response );
                } else {
                    cb( false, response );
                }
            }
        });
    },
    query: function(query, callback){
        sequelize.query(query, { type: Sequelize.QueryTypes.SELECT}).then(function(result){
            callback({
                error: false,
                data: result
            });
        }, function(err){
            callback({
                error: true,
                data: err
            });
        });
    },
    updateTransaction: function( table, newObj, whereObj, transaction, callback ) {
        return table.update(newObj, { where: whereObj, transaction: transaction }, {
        }).then(function( data ) {
            return callback({
                error: false,
                data: data
            })
        }, function( err ){
            return callback({
                error: true,
                data: err
            })
        });
    },
    findOneTransaction: function( table, whereObj, transaction, callback ) {
        return table.findAll({ limit: 1, where: whereObj, transaction: transaction, lock: transaction.LOCK.UPDATE }).then(function(projects) {
            let sendProjects;
            if( projects[0] ) {
                sendProjects = projects[0];
            } else {
                sendProjects = {};
            }
            return callback({
                error: false,
                data: sendProjects
            })
        }, function( err ){
            return callback({
                error: true,
                data: err
            })
        });
    },
    Sequelize: sequelize,
    users
};


module.exports = sendExports;
